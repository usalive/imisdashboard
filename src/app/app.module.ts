import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { DataTablesModule } from 'angular-datatables';

/* Services */
import { AdvertiserService } from './services/advertiser.service';
import { DashBoardService } from './services/dash-board.service';
import { IssueDateService } from './services/issue-date.service';
import { MediaAssetService } from './services/media-asset.service';
import { MediaOrderService } from './services/media-order.service';

import { AppComponent } from './app.component';

import { AdvertiserTabComponent } from './components/advertiser-tab/advertiser-tab.component';
import { MediaAssetTabComponent } from './components/media-asset-tab/media-asset-tab.component';
import { ProposalOrderTabComponent } from './components/proposal-order-tab/proposal-order-tab.component';
import { CookieService } from 'ngx-cookie-service';

/** States */

@NgModule({
  declarations: [
    AppComponent,
    AdvertiserTabComponent,
    MediaAssetTabComponent,
    ProposalOrderTabComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NgSelectModule,
    HttpModule,
    HttpClientModule,
    DataTablesModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    ToastrModule.forRoot({
      preventDuplicates: true,
      timeOut: 5000,
      closeButton: true,
      positionClass: 'toast-top-right',
      progressBar: true,
      enableHtml: true
    }),
  ],
  providers: [
    AdvertiserService,
    DashBoardService,
    IssueDateService,
    MediaAssetService,
    MediaOrderService,
    CookieService 
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
