import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { environment } from '../environments/environment';

import { MediaAssetService } from './services/media-asset.service';
import { IssueDateService } from './services/issue-date.service';
import { MediaOrderService } from './services/media-order.service';
import { DashBoardService } from './services/dash-board.service';

import { ToastrService, ToastrModule } from 'ngx-toastr';
import * as alasql from 'alasql';

declare var c3: any;
declare var d3: any;


@Component({
  selector: 'asi-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {


  ordersByStatusForm: FormGroup;

  baseUrl = '';
  websiteRoot = '';
  reqVerificationToken = '';
  imgurl = '';
  loadTime = 'onLoadTime';
  errorMessage = 'Something went wrong !';

  mediaAssetsList: any[] = [];
  issueDatesList: any[] = [];
  orderByStatus: any[] = [];
  orderByYearly: any[] = [];
  orderByPCYearly = [];
  monthWiseOrderRevenue = [];

  allYearOrders: any = {};
  currentYearOrders: any = {};
  previousYearOrders: any = {};

  insertionOrderCountPreviousYear = '0';
  insertionOrderCountCurrentYear = '0';
  totalNetAmountPreviousYear = '0';
  totalNetAmountCurrentYear = '0';
  mediaAssetUsedInOrderPreviousYear = '0';
  mediaAssetUsedInOrderCurrentYear = '0';
  advertiserUsedInOrderPreviousYear = '0';
  advertiserUsedInOrderCurrentYear = '0';

  mediaAssetTab = false;
  advertiserTab = true;
  proposalOrderTab = false;
  proposalOrderDisable = false;
  mediaAssetTabDisable = false;
  mediaAssetDisable = false;
  advertiserDisable = false;
  isLoading = true;


  constructor(private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private mediaAssetService: MediaAssetService,
    private issueDateService: IssueDateService,
    private mediaOrderService: MediaOrderService,
    private dashBoardService: DashBoardService) {
    ToastrModule.forRoot();
  }

  ngOnInit() {
    const url = window.location.href;
    if (url.search('Advertiser') > 1) {
      this.setAdvertisersTabActive();
    } else if (url.search('MediaAsset') > 1) {
      this.setMediaAssetsTabActive();
    } else if (url.search('ProposalOrder') > 1) {
      this.setProposalOrderTabActive();
    } else {
      this.setAdvertisersTabActive();
    }
    this.getToken();

    this.ordersByStatusFormValidation();

    this.getDashboardData();
    this.getDataOnChangeofDropdown(this.loadTime);
  }

  ngAfterViewInit() {
  }

  getToken() {
    try {
      this.baseUrl = environment.baseUrl;
      this.websiteRoot = environment.websiteRoot;
      var pathArray = this.websiteRoot.split( '/' );
      var protocol = pathArray[0];
      var host = pathArray[2];
      this.websiteRoot = protocol + '//' + host + '/';
      if (this.websiteRoot.startsWith("http://"))
      this.websiteRoot = this.websiteRoot.replace('http', 'https');
      this.reqVerificationToken = environment.token;
      this.imgurl = environment.imageUrl;
    } catch (error) {
      console.log(error);
    }
  }

  ordersByStatusFormValidation() {
    this.ordersByStatusForm = this.formBuilder.group({
      'MediaAssetId': [null, Validators.required],
      'IssueDateId': [null, Validators.required]
    });
  }

  getDashboardData() {
    try {
      this.dashBoardService.getDashboardData().subscribe(result => {
        if (result.StatusCode === 1) {
          if (result.Data != null) {

            const dashBoardTotalNetCostData = result.Data.TotalNetCostModel;
            this.getTotalNetAmount(dashBoardTotalNetCostData);

            const dashBoardInserationOrderCountData = result.Data.InserationOrderCountModel;
            this.getInserationOrderCount(dashBoardInserationOrderCountData);

            const dashBoardMediaAssetCountData = result.Data.UsedMediaAssetCountModel;
            this.getMediaAssetUsedInOrder(dashBoardMediaAssetCountData);

            const dashBoardAdvertiserCountData = result.Data.UsedAdvertiserCountModel;
            this.getAdvertiserUsedInOrder(dashBoardAdvertiserCountData);

            const dashBoardOrderCountAndRevenueByMonthWiseData = result.Data.OrderCountAndRevenueByMonthWise;
            this.getOrderCountbyIssueForCurrentYear(dashBoardOrderCountAndRevenueByMonthWiseData);
            this.getOrderRevenueSumByIssueForCurrentYear(dashBoardOrderCountAndRevenueByMonthWiseData);

            const dashBoardOrderCountByMonthAndYearData = result.Data.OrderCountByMonthAndYear;
            this.getOrderCountByYearly(dashBoardOrderCountByMonthAndYearData);

            const dashBoardOrderByStatusData = result.Data.OrderCountByStatus;
            this.getInserationOrderByStatus(dashBoardOrderByStatusData);

          }
        } else if (result.StatusCode === 3) {
        } else {
          this.toastr.error(result.Message, 'Error!');
        }

      }, error => {
        this.isLoading = false;
        console.log(error);
      });
    } catch (error) {
      this.isLoading = false;
      console.log(error);
    }
  }

  getDataOnChangeofDropdown(_loadTime = null) {
    try {
      const data = {
        MediaAssetId: this.ordersByStatusForm.controls['MediaAssetId'].value,
        IssueDateId: this.ordersByStatusForm.controls['IssueDateId'].value,
        Status: '',
        IsCurrentYear: false
      };

      this.isLoading = true;
      this.mediaOrderService.getDataForPostOrdersDropDown(data).subscribe(result => {
        if (result.StatusCode === 1) {
          this.isLoading = false;
          if (result.Data != null) {
            if (result.Data.length > 0) {
              const apiData = result.Data;
              const iDates = apiData[0].IssueDates;
              let mAssets = apiData[0].MediaAssets;
              if (mAssets.length > 0) {
                if (_loadTime === 'onLoadTime') {
                  mAssets = alasql('SELECT * FROM ? AS add order by MediaAssetName asc', [mAssets]);
                  this.mediaAssetsList = mAssets;
                }
              } else if (_loadTime === 'onLoadTime') {
                this.mediaAssetsList = [];
              }

              // set Issue Date Dropdown
              if (iDates.length > 0) {

                const issueDate = iDates;
                const orderByIssueDate = alasql('SELECT * FROM ? AS add order by CoverDate desc', [issueDate]);
                this.issueDatesList = orderByIssueDate;
                this.ordersByStatusForm.controls['IssueDateId'].setValue(null);

              } else {
                this.issueDatesList = [];
                this.ordersByStatusForm.controls['IssueDateId'].setValue(null);
              }
            }
          }
        } else {
          this.isLoading = false;
        }
      }, error => {
        this.isLoading = false;
        console.log(error);
      });
    } catch (error) {
      this.isLoading = false;
      console.log(error);
    }
  }


  // --- Media & issuedate list ---
  getAllMediaAssets() {
    try {
      this.mediaAssetService.getAll().subscribe(result => {
        if (result.StatusCode === 1) {
          if (result.Data != null) {
            if (result.Data.length > 0) {
              this.mediaAssetsList = result.Data;
            } else {
              this.mediaAssetsList = [];
            }
          } else {
            this.mediaAssetsList = [];
          }
        } else {
          this.mediaAssetsList = [];
        }
      }, error => {
        console.log(error);
      });
    } catch (error) {
      this.isLoading = false;
      console.log(error);
    }
  }

  getAllIssueDates() {
    try {
      this.issueDateService.getAll().subscribe(result => {
        if (result.StatusCode === 1) {
          if (result.Data != null) {
            if (result.Data.length > 0) {
              const issueDate = result.Data;
              const orderByIssueDate = alasql('SELECT * FROM ? AS add order by CoverDate desc', [issueDate]);
              this.issueDatesList = orderByIssueDate;
            } else {
              this.issueDatesList = [];
            }
          } else {
            this.issueDatesList = [];
          }
        } else {
          this.issueDatesList = [];
        }
      }, error => {
        console.log(error);
      });
    } catch (error) {
      this.isLoading = false;
      console.log(error);
    }
  }


  // ----- getDashboardData Functions ------
  getTotalNetAmount(dashBoardTotalNetCostCountData) {
    try {
      if (dashBoardTotalNetCostCountData.length > 0) {
        if (dashBoardTotalNetCostCountData[0].PreviousYear.length > 0) {
          this.totalNetAmountPreviousYear = parseFloat(dashBoardTotalNetCostCountData[0].PreviousYear[0].NetCost).toFixed(2);
          const previousYearNetAmount = (parseFloat(this.totalNetAmountPreviousYear));
          const options2 = { style: 'currency', currency: 'USD' };
          this.totalNetAmountPreviousYear = previousYearNetAmount.toLocaleString('en-US', options2);
        } else {
          this.totalNetAmountPreviousYear = '0';
        }
        if (dashBoardTotalNetCostCountData[0].CurrentYear.length > 0) {
          this.totalNetAmountCurrentYear = parseFloat(dashBoardTotalNetCostCountData[0].CurrentYear[0].NetCost).toFixed(2);
          const CurrentYearNetAmount = (parseFloat(this.totalNetAmountCurrentYear));
          const options2 = { style: 'currency', currency: 'USD' };
          this.totalNetAmountCurrentYear = CurrentYearNetAmount.toLocaleString('en-US', options2);
        } else {
          this.totalNetAmountCurrentYear = '0';
        }
      } else {
        this.totalNetAmountPreviousYear = '0';
        this.totalNetAmountCurrentYear = '0';
      }

    } catch (error) {
      this.isLoading = false;
      console.log(error);
    }
  }

  getInserationOrderCount(dashBoardInserationOrderCountData) {
    try {

      if (dashBoardInserationOrderCountData.length > 0) {

        if (dashBoardInserationOrderCountData[0].PreviousYear.length > 0) {
          this.insertionOrderCountPreviousYear = dashBoardInserationOrderCountData[0].PreviousYear[0].InserationOrderCount;
        } else {
          this.insertionOrderCountPreviousYear = '0';
        }

        if (dashBoardInserationOrderCountData[0].CurrentYear.length > 0) {
          this.insertionOrderCountCurrentYear = dashBoardInserationOrderCountData[0].CurrentYear[0].InserationOrderCount;
        } else {
          this.insertionOrderCountCurrentYear = '0';
        }

      } else {
        this.insertionOrderCountPreviousYear = '0';
        this.insertionOrderCountCurrentYear = '0';
      }
    } catch (error) {
      console.log(error);
      this.isLoading = false;
    }
  }

  getMediaAssetUsedInOrder(dashBoardMediaAssetCountData) {
    try {

      if (dashBoardMediaAssetCountData.length > 0) {

        if (dashBoardMediaAssetCountData[0].PreviousYear.length > 0) {
          this.mediaAssetUsedInOrderPreviousYear = dashBoardMediaAssetCountData[0].PreviousYear[0].MediaAssetCount;
        } else {
          this.mediaAssetUsedInOrderPreviousYear = '0';
        }

        if (dashBoardMediaAssetCountData[0].CurrentYear.length > 0) {
          this.mediaAssetUsedInOrderCurrentYear = dashBoardMediaAssetCountData[0].CurrentYear[0].MediaAssetCount;
        } else {
          this.mediaAssetUsedInOrderCurrentYear = '0';
        }

      } else {
        this.mediaAssetUsedInOrderPreviousYear = '0';
        this.mediaAssetUsedInOrderCurrentYear = '0';
      }

    } catch (error) {
      this.isLoading = false;
      console.log(error);
    }
  }

  getAdvertiserUsedInOrder(dashBoardAdvertiserCountData) {
    try {

      if (dashBoardAdvertiserCountData.length > 0) {

        if (dashBoardAdvertiserCountData[0].PreviousYear.length > 0) {
          this.advertiserUsedInOrderPreviousYear = dashBoardAdvertiserCountData[0].PreviousYear[0].AdvertiserCount;
        } else {
          this.advertiserUsedInOrderPreviousYear = '0';
        }

        if (dashBoardAdvertiserCountData[0].CurrentYear.length > 0) {
          this.advertiserUsedInOrderCurrentYear = dashBoardAdvertiserCountData[0].CurrentYear[0].AdvertiserCount;
        } else {
          this.advertiserUsedInOrderCurrentYear = '0';
        }

      } else {
        this.advertiserUsedInOrderPreviousYear = '0';
        this.advertiserUsedInOrderCurrentYear = '0';
      }
    } catch (error) {
      this.isLoading = false;
      console.log(error);
    }
  }

  getOrderCountbyIssueForCurrentYear(dashBoardOrderCountAndRevenueByMonthWiseData) {
    try {

      if (dashBoardOrderCountAndRevenueByMonthWiseData.length > 0) {
        this.orderByYearly = dashBoardOrderCountAndRevenueByMonthWiseData;
        this.generateBarChart();
      } else {
        this.orderByYearly = [];
        const data = [];
        this.generateBarChart();
      }

    } catch (error) {
      this.isLoading = false;
      console.log(error);
    }
  }

  getOrderRevenueSumByIssueForCurrentYear(dashBoardOrderCountAndRevenueByMonthWiseData) {
    try {
      if (dashBoardOrderCountAndRevenueByMonthWiseData.length > 0) {

        this.orderByYearly = dashBoardOrderCountAndRevenueByMonthWiseData;
        const allMonth = [
          'January', 'February', 'March', 'April', 'May', 'June',
          'July', 'August', 'September', 'October', 'November', 'December'
        ];
        const tmpMonthWiseData = [];
        const tmpMonthWiseRevenue = [];
        const dataLength = this.orderByYearly.length - 1;

        tmpMonthWiseData.push('No of orders ');
        this.orderByYearly.forEach(element => {
          tmpMonthWiseData[element.MonthName] = element.NoOfOrders;
          tmpMonthWiseRevenue[element.MonthName] = element.Revenue;
        });

        const monthWiseRevenue = [];
        monthWiseRevenue.push('Revenue ');

        allMonth.forEach(element => {
          const isMonthExits = tmpMonthWiseData[element];
          const monthIndex = this.getMonthIndex(element);
          if (monthIndex > 0) {
            if (isMonthExits !== undefined && isMonthExits != null) {
              monthWiseRevenue[monthIndex] = tmpMonthWiseRevenue[element];
            } else {
              monthWiseRevenue[monthIndex] = 0;
            }
          }
        });

        this.monthWiseOrderRevenue = monthWiseRevenue;
        this.generateBarChartforOrderRevenue();
      } else {
        this.orderByYearly = [];
        this.generateBarChartforOrderRevenue();
      }

    } catch (error) {
      this.isLoading = false;
      console.log(error);
    }
  }

  getOrderCountByYearly(dashBoardOrderCountByMonthAndYearData) {
    try {
      if (dashBoardOrderCountByMonthAndYearData.length > 0) {
        this.orderByPCYearly = dashBoardOrderCountByMonthAndYearData;
        this.yearlyOrderBarchart();
      } else {
        this.orderByPCYearly = [];
        this.yearlyOrderBarchart();
      }
    } catch (error) {
      this.isLoading = false;
      console.log(error);
    }
  }

  getInserationOrderByStatus(dashBoardOrderByStatusData) {
    try {
      this.orderByStatus = dashBoardOrderByStatusData;
      if (this.orderByStatus.length > 0) {
        const pieColumns = [];

        this.orderByStatus.forEach(element => {
          const column = [];
          const name = element.Status;
          column[name] = element.NoOfOrders;
          pieColumns.push(column);
        });

        if (pieColumns.length > 0) {
          this.generatePieChart(pieColumns);
        }
      } else {
        this.orderByStatus = [];
        const data = [];
        this.generatePieChart(data);
      }
    } catch (error) {
      this.isLoading = false;
      console.log(error);
    }
  }

  getOrdersbyStatusWithFilter() {
    try {
      const data = {
        MediaAssetId: this.ordersByStatusForm.controls['MediaAssetId'].value,
        IssueDateId: this.ordersByStatusForm.controls['IssueDateId'].value,
      };

      this.dashBoardService.getInserationOrderByStatus(data).subscribe(result => {
        if (result.StatusCode === 1) {
          console.log(result);
          if (result.Data != null) {
            if (result.Data.length > 0) {
              this.orderByStatus = result.Data;
              const pieColumns = [];
              this.orderByStatus.forEach(element => {
                const column = [];
                const name = element.Status;
                column[name] = element.NoOfOrders;
                pieColumns.push(column);
              });
              if (pieColumns.length > 0) {
                this.generatePieChart(pieColumns);
              }
            } else {
              this.orderByStatus = [];
              const datas = [];
              this.generatePieChart(datas);
            }
          } else {
            this.orderByStatus = [];
            const datas = [];
            this.generatePieChart(datas);
          }
        } else if (result.StatusCode === 3) {
          this.orderByStatus = [];
          const datas = [];
          this.generatePieChart(datas);
        } else {
          this.orderByStatus = [];
          const datas = [];
          this.generatePieChart(datas);
          this.toastr.error(result.Message, 'Error!');
        }
      }, error => {
        this.toastr.error(this.errorMessage, '');
        console.log(error);
      });
    } catch (error) {
      this.isLoading = false;
      console.log(error);
    }
  }

  getMonthIndex(monthName) {
    try {
      let index = 0;
      if (monthName === 'January') {
        index = 1;
      } else if (monthName === 'February') {
        index = 2;
      } else if (monthName === 'March') {
        index = 3;
      } else if (monthName === 'April') {
        index = 4;
      } else if (monthName === 'May') {
        index = 5;
      } else if (monthName === 'June') {
        index = 6;
      } else if (monthName === 'July') {
        index = 7;
      } else if (monthName === 'August') {
        index = 8;
      } else if (monthName === 'September') {
        index = 9;
      } else if (monthName === 'October') {
        index = 10;
      } else if (monthName === 'November') {
        index = 11;
      } else if (monthName === 'December') {
        index = 12;
      }
      return index;
    } catch (error) {
      this.isLoading = false;
      console.log(error);
    }
  }

  // dropDown change event
  getOrdersbyStatus() {
    this.getOrdersbyStatusWithFilter();
  }

  generateBarChart() {
    const allMonth = [
      'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
    ];
    const tmpMonthWiseData = [];
    const tmpMonthWiseRevenue = [];
    const dataLength = this.orderByYearly.length - 1;
    tmpMonthWiseData.push('No of orders ');

    this.orderByYearly.forEach(element => {
      tmpMonthWiseData[element.MonthName] = element.NoOfOrders;
      tmpMonthWiseRevenue[element.MonthName] = element.Revenue;
    });

    const monthWiseRevenue = [];
    const monthWiseData = [];
    monthWiseData.push('No of orders ');
    monthWiseRevenue.push('Revenue ');

    allMonth.forEach(element => {
      const isMonthExits = tmpMonthWiseData[element];
      const monthIndex = this.getMonthIndex(element);
      if (monthIndex > 0) {
        if (isMonthExits !== undefined && isMonthExits != null) {
          monthWiseData[monthIndex] = tmpMonthWiseData[element];
          monthWiseRevenue[monthIndex] = tmpMonthWiseRevenue[element];
        } else {
          monthWiseData[monthIndex] = 0;
          monthWiseRevenue[monthIndex] = 0;
        }
      }
    });


    let maxValue = 0;
    if (true) {
      let tmpMonthWiseData2 = monthWiseData;
      tmpMonthWiseData2 = tmpMonthWiseData2.slice(1);
      if (tmpMonthWiseData2.length > 0) {
        const Value = Math.max.apply(Math, tmpMonthWiseData2);
        // tslint:disable-next-line:radix
        if (parseInt(Value) > 0) {
          maxValue = Value;
          maxValue = maxValue + 3;
        }
      }
    }

    this.monthWiseOrderRevenue = monthWiseRevenue;

    const xAxisLabel = ['x', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    const chart = new c3.generate({
      bindto: '#Barchart',
      data: {
        x: 'x',
        columns: [
          xAxisLabel,
          monthWiseData,
          // monthWiseRevenue,
        ],
        type: 'bar',
        onclick: function (d, element) {  },
        onmouseover: function (d) {  },
        onmouseout: function (d) {  }
      },
      axis: {
        x: {
          type: 'categorized',
          tick: {
            width: 0.1,
            rotate: 75,
            multiline: false
          },
          label: {
            text: 'Months',
            position: 'outer-center'
          },
          padding: {
            bottom: 20,
          },
          height: 70
        },
        y: {
          label: {
            text: 'No of orders',
            position: 'outer-center'
          },
          padding: {
            bottom: 20,
          },
          max: maxValue,
          // min: -1,
        }
      },

      bar: {
        width: {
          ratio: 0.7,
          // max: 30
        },
      }
    });
  }

  generatePieChart(data) {
    try {
      let proposal = 0;
      let run = 0;
      let cancel = 0;

      data.forEach(element => {
        const keyName = Object.keys(element)[0];
        if (keyName === 'Cancel') {
          cancel = element['Cancel'];
        } else if (keyName === 'Proposal') {
          proposal = element['Proposal'];
        } else if (keyName === 'Run') {
          run = element['Run'];
        }
      });

      const chart = new c3.generate({
        bindto: '#Piechart',
        data: {
          // iris data from R
          columns: [
            ['Proposal', proposal],
            ['Run', run],
            ['Cancel', cancel],
          ],
          // columns: data,
          type: 'pie',

        },
        legend: {
          show: true
        },

        label: {
          format: function (value, ratio, id) {
            return d3.format('$')(value);
          }
        }
      });

      new d3.select('#chart').insert('div', '.chart').attr('class', 'legend').selectAll('span')

        .on('mouseover', function (id) {
          chart.focus(id);
        })
        .on('mouseout', function (id) {
          chart.revert();
        })
        .on('click', function (id) {
          chart.toggle(id);
        });
    } catch (error) {
      this.isLoading = false;
      console.log(error);
    }
  }

  generateBarChartforOrderRevenue() {
    let maxValue = 0;
    if (true) {
      let tmpMonthWiseData = this.monthWiseOrderRevenue;
      tmpMonthWiseData = tmpMonthWiseData.slice(1);
      if (tmpMonthWiseData.length > 0) {
        const Value = Math.max.apply(Math, tmpMonthWiseData);
        // tslint:disable-next-line:radix
        if (parseInt(Value) > 0) {
          maxValue = Value;
          maxValue = maxValue + 3;
        }
      }
    }

    const revenueData = this.monthWiseOrderRevenue;

    const xAxisLabel = ['x', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    const chart = new c3.generate({
      bindto: '#RevenueBarchart',
      data: {
        x: 'x',
        columns: [
          xAxisLabel,
          revenueData,
          // monthWiseRevenue,
        ],
        type: 'bar',
        color: function (d) {
          return '#df8208';
        },
        onclick: function (d, element) {  },
        onmouseover: function (d) {  },
        onmouseout: function (d) {  }
      },
      axis: {
        x: {
          type: 'categorized',
          tick: {
            width: 0.1,
            rotate: 75,
            multiline: false
          },
          label: {
            text: 'Months ',
            position: 'outer-center'
          },
          padding: {
            bottom: 20,
          },
          height: 70
        },
        y: {
          label: {
            text: 'Revenue ',
            position: 'outer-center'
          },
          padding: {
            bottom: 20,
          },
          max: maxValue,
          // min: -1,
        }
      },

      bar: {
        width: {
          ratio: 0.7,
        },
      }
    });
  }

  yearlyOrderBarchart() {
    let allMonth = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    this.allYearOrders = [];
    this.currentYearOrders = [];
    this.previousYearOrders = [];
    if (this.orderByPCYearly.length > 0) {
        this.allYearOrders = this.orderByPCYearly[0];
        this.currentYearOrders = this.allYearOrders.CurrentYear;
        this.previousYearOrders = this.allYearOrders.PreviousYear;

        var tmpMonthWiseData = [];
        var dataLength = this.currentYearOrders.length - 1;
        tmpMonthWiseData.push('Revenue ');
        
        this.currentYearOrders.forEach((d, k) => {
          tmpMonthWiseData[d.MonthName] = d.Revenue;
        });
        

        var monthWiseData = [];
        monthWiseData.push('Current year revenue ');

        allMonth.forEach((data, key) => {
          var isMonthExits = tmpMonthWiseData[data];
            var monthIndex = this.getMonthIndex(data);
            if (monthIndex > 0) {
                if (isMonthExits != undefined && isMonthExits != null) {
                    monthWiseData[monthIndex] = tmpMonthWiseData[data];
                } else {
                    monthWiseData[monthIndex] = 0;
                }
            }
        });

        var tmpMonthWiseDataForPreviousYr = [];
        var dataLengthPY = this.previousYearOrders.length - 1;
        tmpMonthWiseDataForPreviousYr.push('Revenue ');
        this.previousYearOrders.forEach((d, k) => {
          tmpMonthWiseDataForPreviousYr[d.MonthName] = d.Revenue;
        });

        var monthWiseDataPreviousYr = [];
        monthWiseDataPreviousYr.push('Previous year revenue ');
        allMonth.forEach((data, key) => {
          var isMonthExits = tmpMonthWiseDataForPreviousYr[data];
          var monthIndex = this.getMonthIndex(data);
          if (monthIndex > 0) {
              if (isMonthExits != undefined && isMonthExits != null) {
                  monthWiseDataPreviousYr[monthIndex] = tmpMonthWiseDataForPreviousYr[data];
              } else {
                  monthWiseDataPreviousYr[monthIndex] = 0;
              }
          }
        });

        var maxValue = 0;
        var maxValueCY = 0;
        var maxValuePY = 0;
        if (true) {
            var tmpMonthWiseData = monthWiseData;
            tmpMonthWiseData = tmpMonthWiseData.slice(1);
            if (tmpMonthWiseData.length > 0) {
                var Value = Math.max.apply(Math, tmpMonthWiseData);
                if (parseInt(Value) > 0) {
                    maxValueCY = Value;
                    maxValueCY = maxValueCY + 3;
                }
            }
        }

        if (true) {
            var tmpMonthWiseData = monthWiseDataPreviousYr;
            tmpMonthWiseData = tmpMonthWiseData.slice(1);
            if (tmpMonthWiseData.length > 0) {
                var Value = Math.max.apply(Math, tmpMonthWiseData);
                if (parseInt(Value) > 0) {
                    maxValuePY = Value;
                    maxValuePY = maxValuePY + 3;
                }
            }
        }

        if (maxValueCY > maxValuePY) {
            maxValue = maxValueCY;
        } else {
            maxValue = maxValuePY;
        }

        var xAxisLabel = ['x', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var chart = c3.generate({
            bindto: '#YearlyOrderBarchart',
            data: {
                x: 'x',
                columns: [
                  xAxisLabel,
                   monthWiseDataPreviousYr,
                   monthWiseData,
                ],
                type: 'bar',

                onclick: function (d, element) { },
                onmouseover: function (d) { },
                onmouseout: function (d) { }
            },
            axis: {
                x: {
                    type: 'categorized',
                    tick: {
                        width: 0.1,
                        rotate: 75,
                        multiline: false
                    },
                    label: {
                        text: 'Months',
                        position: 'outer-center'
                    },
                    padding: {
                        bottom: 20,
                    },
                    height: 70
                },
                y: {
                    label: {
                        text: 'Revenue',
                        position: 'outer-center'
                    },
                    padding: {
                        bottom: 20,
                    },
                    max: maxValue,
                    //min: -1,
                }
            },

            bar: {
                width: {
                    ratio: 0.7,
                    //max: 30
                },
            }
        });
    }
}

  // ----- Dropdown change event -------
  onChangeMediaAsset() {
    try {
      const Id = this.ordersByStatusForm.controls['MediaAssetId'].value;
      if (Id !== undefined && Id !== '' && Id !== '0' && Id != null) {
        this.ordersByStatusForm.controls['IssueDateId'].setValue(null);
        this.getDataOnChangeofDropdown();
      } 
      else if (Id === '' || Id === null || Id === undefined) {
        
        this.ordersByStatusForm.controls['MediaAssetId'].setValue(null);
        this.ordersByStatusForm.controls['IssueDateId'].setValue(null);
        this.getDataOnChangeofDropdown();
      }
       else {
      }
    } catch (error) {
      console.log(error);
    }
  }

  onChangeIssueDate() {
    try {
      const Id = this.ordersByStatusForm.controls['IssueDateId'].value;
      if (Id !== undefined && Id !== '' && Id !== '0' && Id != null) {
      } else {
      }
    } catch (error) {
      console.error(error);
    }
  }



  // -------- Tab --------
  setAdvertisersTabActive() {
    let url = window.location.href;

    if (url.indexOf('#') > -1) {
      url = url.substring(0, url.lastIndexOf('#'));
    }

    location.replace(url + '#/DashBoard/Advertiser');

    this.mediaAssetTab = false;
    this.advertiserTab = true;
    this.proposalOrderTab = false;

    this.mediaAssetTabDisable = false;
    this.advertiserDisable = false;
    this.proposalOrderDisable = false;
  }

  setMediaAssetsTabActive() {
    let url = window.location.href;

    if (url.indexOf('#') > -1) {
      url = url.substring(0, url.lastIndexOf('#'));
    }

    location.replace(url + '#/DashBoard/MediaAsset');

    this.mediaAssetTab = true;
    this.advertiserTab = false;
    this.proposalOrderTab = false;

    this.mediaAssetTabDisable = false;
    this.advertiserDisable = false;
    this.proposalOrderDisable = false;
  }

  setProposalOrderTabActive() {
    let url = window.location.href;

    if (url.indexOf('#') > -1) {
      url = url.substring(0, url.lastIndexOf('#'));
    }
    location.replace(url + '#/DashBoard/ProposalOrder');

    this.mediaAssetTab = false;
    this.advertiserTab = false;
    this.proposalOrderTab = true;

    this.mediaAssetTabDisable = false;
    this.advertiserDisable = false;
    this.proposalOrderDisable = false;
  }

}
