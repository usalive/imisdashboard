import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy, Renderer } from '@angular/core';
import { ToastrService, ToastrModule } from 'ngx-toastr';
import { HttpClient, HttpResponse } from '@angular/common/http';
import * as alasql from 'alasql';
import { environment } from '../../../environments/environment';
import { AppComponent } from '../../app.component';
import { MediaAssetService } from '../../services/media-asset.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
declare const jsPDF: any;

@Component({
  selector: 'asi-media-asset-tab',
  templateUrl: './media-asset-tab.component.html',
  styleUrls: ['./media-asset-tab.component.css']
})
export class MediaAssetTabComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  mediaAssetsTableDatatableTrigger: Subject<any> = new Subject();
  mediaAssetsTable: DataTables.Settings = {};
  isMediaAssets_DataLoad_FirstTime = true;


  baseUrl = '';
  apiBaseUrl = '';
  websiteRoot = '';
  reqVerificationToken = '';
  imgurl = '';
  errorMessage = 'Something went wrong !';

  advrertiserData = [];
  partyAndOrganizationData = [];
  tmpPdfAdvrertisers = [];
  tmpAllMediaAssets = [];

  iMISMenuName = 'Advertising';

  obj: any = {};


  @ViewChild('SideBarPanel') SideBarPanel;
  sortingOrder: string;
  reverse: boolean;
  filteredItems: any[];
  groupedItems: any[];
  itemsPerPage: number;
  pagedItems: any[];
  currentPage: number;
  mediaAssetData: any;
  items: any;
  proposalOrder: any;

  constructor(private toastr: ToastrService,
    private http: HttpClient,
    private renderer: Renderer,
    private mediaAssetService: MediaAssetService,
    private appComponent: AppComponent) {
    ToastrModule.forRoot();
  }

  ngOnInit() {
    try {
      this.apiBaseUrl = environment.ApiBaseURL;
      this.baseUrl = environment.baseUrl;
      this.websiteRoot = environment.websiteRoot;
      var pathArray = this.websiteRoot.split( '/' );
      var protocol = pathArray[0];
      var host = pathArray[2];
      this.websiteRoot = protocol + '//' + host + '/';
      if (this.websiteRoot.startsWith("http://"))
      this.websiteRoot = this.websiteRoot.replace('http', 'https');
      this.reqVerificationToken = environment.token;
      this.imgurl = environment.imageUrl;
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
    this.displayOrderDatatableJquery();
  }

  ngAfterViewInit() {
    this.isMediaAssets_DataLoad_FirstTime = false;
    this.mediaAssetsTableDatatableTrigger.next();
  }

  ngOnDestroy() {
    this.isMediaAssets_DataLoad_FirstTime = true;
    this.mediaAssetsTableDatatableTrigger.unsubscribe();
  }


  displayOrderDatatableJquery() {
    try {
      const filterOptions = this.proposalOrder;
      let data = {};
      data = filterOptions;
      this.intiOrderDatable(data);
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  intiOrderDatable(data) {
    try {
      this.mediaAssetsTable = {
        destroy: true,
        pagingType: 'full_numbers',
        pageLength: 10,
        processing: true,
        serverSide: true,
        search: false,
        orderMulti: false,
        responsive: true,
        scrollCollapse: false,
        ordering: false,
        lengthChange: false,
        searching: false,
        ajax: (dataTablesParameters: any, callback) => {
          this.tmpPdfAdvrertisers = [];
          this.tmpAllMediaAssets = [];
          this.mediaAssetService.GetordersformediaAsset(dataTablesParameters, data).subscribe(resp => {
            this.hideLoader();
            this.tmpPdfAdvrertisers = resp.Data;
            this.tmpAllMediaAssets = resp.Data;
            callback({
              recordsTotal: resp.RecordsTotal,
              recordsFiltered: resp.RecordsFiltered,
              data: resp.Data
            });
          }, error => {
            this.hideLoader();
            console.log(error);
          });
        },
        columns: [
        {
          name: 'Media Asset',
          orderable: false,
          render: function (_data, type, row) {
            return row.MediaAssetName;
          },
      },
      {
        name: 'Last Order Date',
        orderable: false,
        render: function (_data, type, row) {
          let d, m, newdate;
          try {
            const dateObj = new Date(row.LastOrderDate);
            m = dateObj.getMonth() + 1;

            if (m.toString().length === 1) {
              m = '0' + m;
            }
            d = dateObj.getDate();
            if (d.toString().length === 1) {
              d = '0' + d;
            }
            const year = dateObj.getFullYear();
            newdate = m + '/' + d + '/' + year;
          } catch (error) {
            this.hideLoader();
            console.log(error);
          }
          return newdate;
        },
      },
      {
        name: 'Total Quantity',
        orderable: false,
        render: function (_data, type, row) {
          return row.TotalQuantity;
          },
      },
      {
        name: 'Revenue',
        orderable: false,
        render: function (_data, type, row) {
          const tempAmount = parseFloat(row.Revenue.toFixed(2));
          const options2 = { style: 'currency', currency: 'USD' };
          return tempAmount.toLocaleString('en-US', options2);
          },
      },
    ],
      };
      if (!this.isMediaAssets_DataLoad_FirstTime) {
        setTimeout(() => {
          try {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.mediaAssetsTableDatatableTrigger.next();
            });
          } catch (error) {
            this.hideLoader();
            console.log(error);
          }
        }, 1000);
        this.hideLoader();
      }

    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // --- Export :: Excel & PDF ---
  exportToFile() {
    const file = (<HTMLSelectElement>document.getElementById('ddlpvExport')).value;
    if (file === 'Excel') {
      this.exporttoExcel();
    } else if (file === 'PDF') {
      this.exportToPdf();
    }
  }

  exporttoExcel() {
    try {
      const MAOrders = [];

      this.tmpAllMediaAssets.forEach((data, key) => {
        this.obj = {};
        this.obj.MediaAssetName = data.MediaAssetName;
        this.obj.LastOrderDate = data.LastOrderDate;
        this.obj.TotalQuantity = data.TotalQuantity;
        this.obj.NetCostAmount = '$' + (parseFloat(data.Revenue)).toFixed(2);
        MAOrders.push(this.obj);
      });
      alasql('SELECT * INTO XLSX("Top Media Assets.xlsx",{headers:true}) FROM ?', [MAOrders]);
    } catch (error) {
      this.hideLoader();
      this.toastr.error(this.errorMessage, '');
    }
  }

  exportToPdf() {
    try {
      const tmpMediaAssetsForAutoTable = [];

      this.tmpAllMediaAssets.forEach((data, key) => {
        this.obj = [];
        this.obj.push(data.MediaAssetName);
        this.obj.push(data.LastOrderDate);
        this.obj.push(data.TotalQuantity);
        this.obj.push('$' + (parseFloat(data.Revenue)).toFixed(2));
        tmpMediaAssetsForAutoTable.push(this.obj);
      });

      const columns = ['Media Asset', 'Last Order Data', 'Total Quantity', 'Revenue'];
      // Only pt supported (not mm or in)
      const doc = new jsPDF('p', 'pt');
      doc.autoTable(columns, tmpMediaAssetsForAutoTable, {
        theme: 'plain'
      });
      doc.save('Top Media Assets.pdf');
    } catch (error) {
      this.hideLoader();
      this.toastr.error(this.errorMessage, '');
    }
  }

  showLoader() {
    this.appComponent.isLoading = true;
  }

  hideLoader() {
    setTimeout(() => {
      this.appComponent.isLoading = false;
    }, 500);
  }

}
