import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy, Renderer } from '@angular/core';
import { ToastrService, ToastrModule } from 'ngx-toastr';
import { HttpClient, HttpResponse } from '@angular/common/http';
import * as alasql from 'alasql';
import { environment } from '../../../environments/environment';
import { AppComponent } from '../../app.component';
import { AdvertiserService } from '../../services/advertiser.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
declare const jsPDF: any;

declare function redToOrderTabByAdvertiser(a, b): any;

@Component({
  selector: 'asi-advertiser-tab',
  templateUrl: './advertiser-tab.component.html',
  styleUrls: ['./advertiser-tab.component.css']
})
export class AdvertiserTabComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  advertiserTableDatatableTrigger: Subject<any> = new Subject();
  advertiserTable: DataTables.Settings = {};
  IsAdvertiser_DataLoad_FirstTime = true;


  baseUrl = '';
  ApiBaseUrl = '';
  websiteRoot = '';
  reqVerificationToken = '';
  imgurl = '';
  errorMessage = 'Something went wrong !';

  advrertiserData = [];
  partyAndOrganizationData = [];
  tmpPdfAdvrertisers = [];

  iMISMenuName = 'Advertising';

  obj: any = {};

  advertiser: Advertiser[];

  @ViewChild('SideBarPanel') SideBarPanel;

  liElements: any = [];

  constructor(private toastr: ToastrService,
    private http: HttpClient,
    private renderer: Renderer,
    private advertiserService: AdvertiserService,
    private appComponent: AppComponent) {
    ToastrModule.forRoot();
  }

  ngOnInit() {
    this.getToken();
    this.displayOrderDatatableJquery();
  }

  ngAfterViewInit() {
    this.IsAdvertiser_DataLoad_FirstTime = false;
    this.advertiserTableDatatableTrigger.next();

    try {
      this.renderer.listenGlobal('document', 'click', (event) => {
        if (event.target.hasAttribute('view-editOrder-id')) {
          const editOrderData = event.target.getAttribute('view-editOrder-id');
          this.redirectToOrderTabByAdvertiser(editOrderData);
        }
      });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  ngOnDestroy() {
    this.IsAdvertiser_DataLoad_FirstTime = true;
    this.advertiserTableDatatableTrigger.unsubscribe();
  }

  getToken() {
    try {
      this.ApiBaseUrl = environment.ApiBaseURL;
      this.baseUrl = environment.baseUrl;
      this.websiteRoot = environment.websiteRoot;
      var pathArray = this.websiteRoot.split( '/' );
      var protocol = pathArray[0];
      var host = pathArray[2];
      this.websiteRoot = protocol + '//' + host + '/';
      if (this.websiteRoot.startsWith("http://"))
      this.websiteRoot = this.websiteRoot.replace('http', 'https');
      this.reqVerificationToken = environment.token;
      this.imgurl = environment.imageUrl;
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  displayOrderDatatableJquery() {
    try {
      const data = [];
      this.intiOrderDatable();
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  intiOrderDatable() {
    try {
      const asiPath = this.websiteRoot + 'api/Party';
      const data: any = [];
      data['AsiPartyAPI'] = asiPath;

      this.advertiserTable = {
        destroy: true,
        pagingType: 'full_numbers',
        pageLength: 10,
        processing: true,
        serverSide: true,
        search: false,
        orderMulti: false,
        responsive: true,
        scrollCollapse: false,
        ordering: false,
        lengthChange: false,   
        searching: false,
        ajax: (dataTablesParameters: any, callback) => {
          this.advertiserService.getOrdersforadvertiser(dataTablesParameters, data).subscribe(resp => {
            this.hideLoader();
            this.tmpPdfAdvrertisers = resp.Data;
            callback({
              recordsTotal: resp.RecordsTotal,
              recordsFiltered: resp.RecordsFiltered,
              data: resp.Data
            });
          }, error => {
            this.hideLoader();
          });
        },
        columns: [
          {
            name: 'Advertiser',
            orderable: false,
            render: function (_data, type, row) {
              let advertiserName = '';
              const adName = row.AdvertiserName;
              if (adName !== undefined && adName !== '' && adName != null) {
                advertiserName = adName;
              } else {
                advertiserName = '';
              }
              // tslint:disable-next-line:max-line-length
              const customHTML = ' <a href="javascript:" view-editOrder-id="' + row.AdvertiserId + '" ng-click="redirectToOrderTabByAdvertiser(' + row.AdvertiserId + ');">' + advertiserName + ' orders</a>';
              return customHTML;
            },
          },
          {
            name: 'No Of Orders',
            orderable: false,
            render: function (_data, type, row) {
              return row.NoOfOrders;
            },
          },
          {
            name: 'Amount',
            orderable: false,
            render: function (_data, type, row) {
              const tempAmount = parseFloat(row.NetCostAmount.toFixed(2));
              const options2 = { style: 'currency', currency: 'USD' };
              return tempAmount.toLocaleString('en-US', options2);
            },
          },
        ],
      };
      if (!this.IsAdvertiser_DataLoad_FirstTime) {
        setTimeout(() => {
          try {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.advertiserTableDatatableTrigger.next();
            });
          } catch (error) {
            this.hideLoader();
            console.log(error);
          }
        }, 1000);
        this.hideLoader();
      }

    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  redirectToOrderTabByAdvertiser(AdvertiserId) {
    this.liElements = $('#SideBarPanel > .slimScrollDiv > nav.sub-nav-body > div > div.RadTreeView.RadTreeView_Orion > ul.rtUL > li.rtLI');
    if (this.liElements.length > 0) {
      for (let i = 0; i < this.liElements.length; i++) {
        var liElement = this.liElements[i];

        if (liElement != null && liElement != undefined && liElement != "") {
          var liDivElement = liElement.children[0];

          if (liDivElement != null && liDivElement != undefined && liDivElement != "") {
            if (liDivElement.childElementCount > 0) {
              var liDivChildrenElement = liDivElement.children[2];

              if (liDivChildrenElement != null && liDivChildrenElement != undefined && liDivChildrenElement != "") {
                var menuText = liDivChildrenElement.innerText;

                if (menuText == this.iMISMenuName) {
                  var advertisingUlElement = liElement.lastElementChild;

                  if (advertisingUlElement != null && advertisingUlElement != undefined && advertisingUlElement != "") {
                    if (advertisingUlElement.childElementCount > 0) {
                      for (let j = 0; j < this.liElements.length; j++) {
                        var orderLiElement = advertisingUlElement.children[j];
                        var menuText = orderLiElement.innerText;
                        menuText = menuText.trim();
                        if (menuText == 'Orders') {
                          var rtMidDiv = orderLiElement.children;
                         
                          if (rtMidDiv.length > 0) {
                            var rtInDiv = rtMidDiv[0].lastElementChild;
                            var orderURLLink = rtInDiv.getAttribute('href');
                            var hkeyIndexof = orderURLLink.indexOf('.aspx?hkey=');
                            var words = '.aspx?hkey=';
                            var wordsLength = words.length;
                            var guildSample = 'd5435643-42d2-42de-bacc-2c85c0ec59f9';
                            var guildSampleLength = guildSample.length;
                            var tempURLUniquestring = orderURLLink.substr(hkeyIndexof + wordsLength, guildSampleLength);

                            var sampleURL = ' ' + this.websiteRoot + 'Staff/Advertising/Orders/Advertising-Order.aspx?hkey=' + tempURLUniquestring + '#%2FdashBoard%2FOrders';
                            if (parseInt(AdvertiserId) > 0) {
                              sampleURL = ' ' + this.websiteRoot + 'Staff/Advertising/Orders/Advertising-Order.aspx?hkey=' + tempURLUniquestring + '#%2FdashBoard%2FOrders%2F' + AdvertiserId + '';
                            } else {
                              sampleURL = ' ' + this.websiteRoot + 'Staff/Advertising/Orders/Advertising-Order.aspx?hkey=' + tempURLUniquestring + '#%2FdashBoard%2FOrders';
                            }
                            this.redirect(sampleURL);
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  redirect(url) {
    const ua = navigator.userAgent.toLowerCase(),
      isIE = ua.indexOf('msie') !== -1,
      version = parseInt(ua.substr(4, 2), 10);

    // Internet Explorer 8 and lower
    if (isIE && version < 9) {
      const link = document.createElement('a');
      link.href = url;
      document.body.appendChild(link);
      link.click();
    } else {
      window.location.href = url;
    }
  }


  // --- Export :: Excel & PDF ---
  exportToFile() {
    const file = (<HTMLSelectElement>document.getElementById('ddlpvExport')).value;
    if (file === 'Excel') {
      this.exporttoExcel();
    } else if (file === 'PDF') {
      this.exportToPdf();
    }
  }

  exporttoExcel() {
    try {
      const advrertisers = [];
      this.tmpPdfAdvrertisers.forEach(element => {
        this.obj = {};
        this.obj.AdvertiserName = element.AdvertiserName;
        this.obj.NoOfOrders = element.NoOfOrders;
        this.obj.NetCostAmount = '$' + (parseFloat(element.NetCostAmount)).toFixed(2);
        advrertisers.push(this.obj);
      });
      alasql('SELECT * INTO XLSX("Top Advertisers.xlsx",{headers:true}) FROM ?', [advrertisers]);
    } catch (error) {
      this.hideLoader();
      this.toastr.error(this.errorMessage, '');
    }
  }

  exportToPdf() {
    try {
      const tmpAdvrertisersForAutoTable = [];
      this.tmpPdfAdvrertisers.forEach(element => {
        this.obj = [];
        this.obj.push(element.AdvertiserName);
        this.obj.push(element.NoOfOrders);
        this.obj.push('$' + (parseFloat(element.NetCostAmount)).toFixed(2));
        tmpAdvrertisersForAutoTable.push(this.obj);
      });
      const columns = ['Advertiser', 'No Of Orders', 'Amount'];
      // Only pt supported (not mm or in)
      const doc = new jsPDF('p', 'pt');
      doc.autoTable(columns, tmpAdvrertisersForAutoTable, {
        theme: 'plain'
      });
      doc.save('Top Advertisers.pdf');
    } catch (error) {
      this.hideLoader();
      this.toastr.error(this.errorMessage, '');
    }
  }



  showLoader() {
    this.appComponent.isLoading = true;
  }

  hideLoader() {
    setTimeout(() => {
      this.appComponent.isLoading = false;
    }, 500);
  }


}

class Advertiser {
  AdvertiserId: string;
  AdvertiserName: string;
  NoOfOrders: number;
  NetCostAmount: string;
}

class DataTablesResponse {
  Data: any[];
  Draw: number;
  RecordsFiltered: number;
  RecordsTotal: number;
}

