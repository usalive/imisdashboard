import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy, Renderer } from '@angular/core';
import { ToastrService, ToastrModule } from 'ngx-toastr';
import { HttpClient, HttpResponse } from '@angular/common/http';
import * as alasql from 'alasql';
import { environment } from '../../../environments/environment';
import { AppComponent } from '../../app.component';
import { MediaAssetService } from '../../services/media-asset.service';
import { ProposalService } from '../../services/proposal.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { load } from '@angular/core/src/render3/instructions';

declare const jsPDF: any;
declare function redToOrderTabByAdvertiser(a, b): any;
declare const $: any;

@Component({
  selector: 'asi-proposal-order-tab',
  templateUrl: './proposal-order-tab.component.html',
  styleUrls: ['./proposal-order-tab.component.css']
})
export class ProposalOrderTabComponent implements OnInit, AfterViewInit, OnDestroy {
  issueDateService: any;
  liElements: any = [];
  iMISMenuName = 'Advertising';
  potForm: FormGroup;

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  proposalTableDatatableTrigger: Subject<any> = new Subject();
  proposalTable: DataTables.Settings = {};
  isMediaAssets_DataLoad_FirstTime = true;

  baseUrl = '';
  apiBaseUrl = '';
  websiteRoot = '';
  reqVerificationToken = '';
  imgurl = '';
  errorMessage = 'Something went wrong !';

  isShowDefaultTable = false;
  isShowPagination = false;
  dataLoading = true;

  mediaAssets = [];
  issueDates: any = [];
  partyAndOrganizationData = [];
  proposalOrders = [];
  proposalOrder: any = {};

  tmpAllProposalOrders = [];

  mediaAssetSelect = {
    multiple: false,
    formatSearching: 'Searching the media asset...',
    formatNoMatches: 'No media asset found'
  };

  issueDateSelect = {
    multiple: false,
    formatSearching: 'Searching the issue date...',
    formatNoMatches: 'No issue date found'
  };


  obj: any = {};
  tmpProposalOrdersForAutoTable = [];
  selectedIssueDate: any;

  constructor(private toastr: ToastrService,
    private http: HttpClient,
    private renderer: Renderer,
    private formBuilder: FormBuilder,
    private proposalService: ProposalService,
    private mediaAssetService: MediaAssetService,
    private appComponent: AppComponent,
    private cookieService: CookieService) {
    ToastrModule.forRoot();
  }

  ngOnInit() {
    try {
      this.apiBaseUrl = environment.ApiBaseURL;
      this.baseUrl = environment.baseUrl;
      this.websiteRoot = environment.websiteRoot;
      var pathArray = this.websiteRoot.split( '/' );
      var protocol = pathArray[0];
      var host = pathArray[2];
      this.websiteRoot = protocol + '//' + host + '/';
      if (this.websiteRoot.startsWith("http://"))
      this.websiteRoot = this.websiteRoot.replace('http', 'https');
      this.reqVerificationToken = environment.token;
      this.imgurl = environment.imageUrl;
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
    this.potFormValidation();
    const loadTime = 'onLoadTime';
    this.getDataOnChangeofDropdown(loadTime);
    this.displayOrdersWithProposalStatus();
    this.displayOrderDatatableJquery();
  }

  potFormValidation() {
    this.potForm = this.formBuilder.group({
      MediaAsset: [null],
      IssueDate: [null],
    });
  }

  ngAfterViewInit() {
    this.isMediaAssets_DataLoad_FirstTime = false;
    this.proposalTableDatatableTrigger.next();

    try {
      this.renderer.listenGlobal('document', 'click', (event) => {
        if (event.target.hasAttribute('view-editOrder-id')) {
          const editOrderData = event.target.getAttribute('view-editOrder-id');
          this.editOrder(editOrderData);
        }
      });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  ngOnDestroy() {
    this.isMediaAssets_DataLoad_FirstTime = true;
    this.proposalTableDatatableTrigger.unsubscribe();
  }

  getDate(dateTime) {
    const coverDate_yy = new Date(dateTime).getFullYear();
    const coverDate_mm = new Date(dateTime).getMonth() + 1;
    const coverDate_dd = new Date(dateTime).getDate();
    const coverDate = coverDate_mm + '/' + coverDate_dd + '/' + coverDate_yy;
    return coverDate;
  }

  getDataOnChangeofDropdown(_loadTime = null) {
    try {
      const data = {
        MediaAssetId: this.potForm.controls['MediaAsset'].value,
        IssueDateId: this.potForm.controls['IssueDate'].value,
        Status: 'Proposal',
        IsCurrentYear: true
      };

      this.showLoader();
       this.proposalService.getDataForPostOrdersDropDown(data, this.reqVerificationToken).subscribe(result => {
        if (result.StatusCode === 1) {
          this.hideLoader();
          if (result.Data != null) {
            if (result.Data.length > 0) {
              const apiData = result.Data;
              const iDates = apiData[0].IssueDates;
              let mAssets = apiData[0].MediaAssets;
              if (mAssets.length > 0) {
                if (_loadTime === 'onLoadTime') {
                  mAssets = alasql('SELECT * FROM ? AS add order by MediaAssetName asc', [mAssets]);
                  this.mediaAssets = mAssets;
                }
              } else if (_loadTime === 'onLoadTime') {
                this.mediaAssets = [];
              }

              // set Issue Date Dropdown
              if (iDates.length > 0) {

                const issueDate = iDates;
                const orderByIssueDate = alasql('SELECT * FROM ? AS add order by CoverDate desc', [issueDate]);
                this.issueDates = orderByIssueDate;
                this.potForm.controls['IssueDate'].setValue(null);

              } else {
                this.issueDates = [];
                this.potForm.controls['IssueDate'].setValue(null);
              }
            }
          }
        } else {
          this.hideLoader();
        }
      }, error => {
        this.hideLoader();
        console.log(error);
      });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  getAllIssueDates() {
    try {
      this.issueDateService.getAll().subscribe(result => {
        if (result.StatusCode == 1) {
          if (result.Data != null) {
            if (result.Data.length > 0) {
              var issueDate = result.Data;
              var orderByIssueDate = alasql('SELECT * FROM ? AS add order by IssueDateId desc', [issueDate]);
              this.issueDates = orderByIssueDate;
            } else {
              this.issueDates = [];
            }
          } else {
            this.issueDates = [];
          }
        }
        else if (result.StatusCode == 3) {
          this.issueDates = [];
        }
        else {
          this.issueDates = [];
        }
      });
    }
    catch (e) {
      console.log(e);
    }
  }

  onChangeMediaAsset() {
    try {
      const Id = this.potForm.controls['MediaAsset'].value;
      if (Id !== undefined && Id !== '' && Id !== '0' && Id != null) {
        this.potForm.controls['IssueDate'].setValue(null);
        this.getDataOnChangeofDropdown();
      } 
      else if (Id === '' || Id === null || Id === undefined) {
        
        this.potForm.controls['MediaAsset'].setValue(null);
        this.potForm.controls['IssueDate'].setValue(null);
        this.getDataOnChangeofDropdown();
      }
       else {
      }
    } catch (error) {
      console.log(error);
    }
  }
  
  onChangeIssueDate() {
    try {
      const getSelectedData = this.potForm.controls['IssueDate'].value;
      if (getSelectedData != null && getSelectedData !== undefined) {
        const Id = getSelectedData;
        if (Id !== undefined && Id !== '' && Id !== 0 && Id != null) {
        } else {
        }
      }
    } catch (e) {
      console.log(e);
    }
  }

  editOrder(objPOrder) {

    this.liElements = $('#SideBarPanel > .slimScrollDiv > nav.sub-nav-body > div > div.RadTreeView.RadTreeView_Orion > ul.rtUL > li.rtLI');

    if (this.liElements.length > 0) {
      for (let i = 0; i < this.liElements.length; i++) {
        var liElement = this.liElements[i];

        if (liElement != null && liElement != undefined && liElement != "") {
          var liDivElement = liElement.children[0];

          if (liDivElement != null && liDivElement != undefined && liDivElement != "") {
            if (liDivElement.childElementCount > 0) {
              var liDivChildrenElement = liDivElement.children[2];

              if (liDivChildrenElement != null && liDivChildrenElement != undefined && liDivChildrenElement != "") {
                var menuText = liDivChildrenElement.innerText;

                if (menuText == this.iMISMenuName) {
                  var advertisingUlElement = liElement.lastElementChild;

                  if (advertisingUlElement != null && advertisingUlElement != undefined && advertisingUlElement != "") {
                    if (advertisingUlElement.childElementCount > 0) {

                      for (let j = 0; j < this.liElements.length; j++) {
                        var orderLiElement = advertisingUlElement.children[j];

                        var menuText = orderLiElement.innerText;
                        menuText = menuText.trim();

                        if (menuText === 'Orders') {
                          var rtMidDiv = orderLiElement.children;

                          if (rtMidDiv.length > 0) {
                            var rtInDiv = rtMidDiv[0].lastElementChild;
                            var orderURLLink = rtInDiv.getAttribute('href');

                            var hkeyIndexof = orderURLLink.indexOf('.aspx?hkey=');
                            var words = '.aspx?hkey=';
                            var wordsLength = words.length;
                            var guildSample = 'd5435643-42d2-42de-bacc-2c85c0ec59f9';
                            var guildSampleLength = guildSample.length;
                            var tempURLUniquestring = orderURLLink.substr(hkeyIndexof + wordsLength, guildSampleLength);

                            sessionStorage.setItem('OrderId', objPOrder);
                            var sampleURL = '' + this.websiteRoot + 'Staff/Advertising/Orders/Advertising-Order.aspx?hkey=' + tempURLUniquestring + '#%2FordersDetails';

                            this.redirect(sampleURL);
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  redirect(url) {
    const ua = navigator.userAgent.toLowerCase(), isIE = ua.indexOf('msie') !== -1, version = parseInt(ua.substr(4, 2), 10);

    // Internet Explorer 8 and lower
    if (isIE && version < 9) {
      const link = document.createElement('a');
      link.href = url;
      document.body.appendChild(link);
      link.click();
    } else {
      window.location.href = url;
    }
  }

  createListForProposalOrders(objProposalOrders) {
    if (objProposalOrders != null && objProposalOrders !== undefined && objProposalOrders !== '' && objProposalOrders.length > 0) {
      if (objProposalOrders.length > 0) {

        const distintStIds = alasql('SELECT DISTINCT ST_ID  FROM ? AS add ', [objProposalOrders]);
        const distintAdvertiserIds = alasql('SELECT DISTINCT AdvertiserId  FROM ? AS add ', [objProposalOrders]);

        const Ids = [];
        const advertiserIdsTotalLength = distintAdvertiserIds.length;
        const advertiserIdsNoOfTimes = Math.ceil(advertiserIdsTotalLength / 100);
        let adataIndex = 0;
        for (let i = 0; i <= advertiserIdsNoOfTimes - 1; i++) {
          let acurrentIndex = 0;

          distintAdvertiserIds.forEach((data, key) => {
            if (acurrentIndex < 100) {
              if ((adataIndex + 1) <= advertiserIdsTotalLength) {
                // tslint:disable-next-line:radix
                if (parseInt(distintAdvertiserIds[adataIndex].AdvertiserId) > 0
                  && distintAdvertiserIds[adataIndex].AdvertiserId !== ''
                  && distintAdvertiserIds[adataIndex].AdvertiserId != null
                  && distintAdvertiserIds[adataIndex].AdvertiserId !== undefined) {
                  Ids.push(distintAdvertiserIds[adataIndex].AdvertiserId);
                }
                acurrentIndex = acurrentIndex + 1;
                adataIndex = adataIndex + 1;
              }
            }
          });
        }

        const stIdsTotalLength = distintStIds.length;
        const stIdsNoOfTimes = Math.ceil(stIdsTotalLength / 100);
        let sdataIndex = 0;
        for (let i = 0; i <= stIdsNoOfTimes - 1; i++) {
          let scurrentIndex = 0;
          distintStIds.forEach(element => {
            if (scurrentIndex < 100) {
              if ((sdataIndex + 1) <= stIdsTotalLength) {
                // tslint:disable-next-line:radix
                if (parseInt(distintStIds[sdataIndex].ST_ID) > 0
                  && distintStIds[sdataIndex].ST_ID !== ''
                  && distintStIds[sdataIndex].ST_ID != null
                  && distintStIds[sdataIndex].ST_ID !== undefined) {
                  Ids.push(distintStIds[sdataIndex].ST_ID);
                }
                scurrentIndex = scurrentIndex + 1;
                sdataIndex = sdataIndex + 1;
              }
            }
          });
        }

        // get party By Ids
        if (Ids.length > 0) {
          const totalLength = Ids.length;
          const noOfTimes = Math.ceil(totalLength / 100);
          let dataIndex = 0;
          for (let i = 0; i <= noOfTimes - 1; i++) {
            let currentIndex = 0;
            let partyIds = '';

            Ids.forEach((data, key) => {
              if (currentIndex < 100) {
                if ((dataIndex + 1) <= totalLength) {
                  // tslint:disable-next-line:radix
                  if (parseInt(Ids[dataIndex]) > 0
                    && Ids[dataIndex] !== ''
                    && Ids[dataIndex] != null
                    && Ids[dataIndex] !== undefined) {
                    partyIds = partyIds + Ids[dataIndex] + '|';
                  }
                  currentIndex = currentIndex + 1;
                  dataIndex = dataIndex + 1;
                }
              }
            });

            if (partyIds !== '' && partyIds !== undefined && partyIds != null) {
              this.proposalService.getAdvertisersByIds(this.websiteRoot, this.reqVerificationToken,
                partyIds).subscribe(result => {
                  if (result != null && result !== undefined && result !== '') {
                    const itemData = result.Items.$values;
                   
                    if (itemData.length > 0) {
                      this.proposalOrders.forEach((data, key) => {
                        const advertiserId = data.AdvertiserId;
                        const ST_ID = data.ST_ID;
                        const objAdvertiser = alasql('SELECT * FROM ? AS add WHERE Id = ?', [itemData, advertiserId]);
                        const objContact = alasql('SELECT * FROM ? AS add WHERE Id = ?', [itemData, ST_ID]);
                        if (objAdvertiser.length > 0) {
                          this.proposalOrders[key].AdvertiserName = objAdvertiser[0].Name;
                        } else {
                        }
                        if (objContact.length > 0) {
                          this.proposalOrders[key].billToContactName = objContact[0].Name;
                        } else {
                        }
                      });
                      this.proposalOrders = alasql('SELECT * FROM ? AS add ORDER BY AdvertiserName ASC',
                        [this.proposalOrders]);
                      this.tmpAllProposalOrders = $.copy(this.proposalOrders);
                      this.isShowDefaultTable = false;
                      this.dataLoading = false;
                      this.isShowPagination = true;

                    }
                  }
                });
            }
          }
        }

      } else {
        this.proposalOrders = [];
        this.isShowDefaultTable = true;
        this.isShowPagination = false;
        this.dataLoading = false;
      }
    }
  }

  searchOrdersbyProposalStatus() {
    const data = this.proposalOrder;
    this.proposalService.getProposalStatus(data, this.reqVerificationToken).subscribe(result => {
      if (result.StatusCode === 1) {
        if (result.Data != null) {
          if (result.Data.length > 0) {
            this.proposalOrders = result.Data;
            this.createListForProposalOrders(this.proposalOrders);
          } else {
            this.proposalOrders = [];
            this.isShowDefaultTable = true;
            this.isShowPagination = false;
            this.dataLoading = false;
          }
        } else {
          this.proposalOrders = [];
          this.isShowDefaultTable = true;
          this.isShowPagination = false;
          this.dataLoading = false;
        }
      } else if (result.StatusCode === 3) {
        this.proposalOrders = [];
        this.isShowDefaultTable = true;
        this.isShowPagination = false;
        this.dataLoading = false;
      } else {
        this.proposalOrders = [];
        this.isShowDefaultTable = true;
        this.isShowPagination = false;
        this.dataLoading = false;
        this.toastr.error(result.Message, 'Error!');
      }
    });
  }

  displayOrderDatatableJquery() {
    try {
      const asiPath = this.websiteRoot + 'api/Party';
      const data = [];
      data['AsiPartyAPI'] = asiPath;
      data['MediaAssetId'] = this.potForm.controls['MediaAsset'].value;
      data['IssueDateId'] = this.potForm.controls['IssueDate'].value;
      this.intiOrderDatable(data);
    } catch (err) {
      this.hideLoader();
      console.log(err);
    }
  }

  displayOrdersWithProposalStatus() {
    const asiPath = this.websiteRoot + 'api/Party';
    const data = [];
    data['AsiPartyAPI'] = asiPath;
    this.intiOrderDatable(data);
  }

  intiOrderDatable(data) {
    try {
      const root = this.websiteRoot;
      const imgurl = this.imgurl;
      this.proposalTable = {
        destroy: true,
        pagingType: 'full_numbers',
        pageLength: 10,
        processing: true,
        serverSide: true,
        search: false,
        orderMulti: false,
        responsive: true,
        scrollCollapse: false,
        ordering: false,
        lengthChange: false, 
        searching: false,
        ajax: (dataTablesParameters: any, callback) => {
          this.tmpAllProposalOrders = [];
          this.proposalService.getOrdersbyProposalStatus(dataTablesParameters, data).subscribe(resp => {
            this.hideLoader();
            this.tmpAllProposalOrders = resp.Data;
          
            callback({
              recordsTotal: resp.RecordsTotal,
              recordsFiltered: resp.RecordsFiltered,
              data: resp.Data
            });
          }, error => {
            this.hideLoader();
            console.log(error);
          });
        },
        columns: [
          {
            name: 'Buy Id',
            orderable: false,
            render: function (_data, type, row) {
              return row.BuyId;
            },
          },
          {
            name: 'Advertiser',
            orderable: false,
            render: function (_data, type, row) {
              let advertiserName = '';
              const adName = row.AdvertiserName;
              if (adName !== undefined && adName !== '' && adName != null) {
                advertiserName = adName;
              } else {
                advertiserName = '';
              }
              return advertiserName;
            },
          },
          {
            name: 'Billing Contact',
            orderable: false,
            render: function (_data, type, row) {
              let BillingContact = '';
              const adName = row.BillToContactName;
              if (adName !== undefined && adName !== '' && adName != null) {
                BillingContact = adName;
              } else {
                BillingContact = '';
              }

              return BillingContact;
            },
          },
          {
            name: 'Status',
            orderable: false,
            render: function (_data, type, row) {
              return row.OrderStatus;
            },
          },
          {
            name: 'Gross Cost',
            orderable: false,
            render: function (_data, type, row) {
              const tempAmount = parseFloat(row.GrossCost.toFixed(2));
              const options2 = { style: 'currency', currency: 'USD' };
              return tempAmount.toLocaleString('en-US', options2);
            },
          },
          {
            name: 'Net Cost',
            orderable: false,
            render: function (_data, type, row) {
              const tempAmount = parseFloat(row.NetCost.toFixed(2));
              const options2 = { style: 'currency', currency: 'USD' };
              return tempAmount.toLocaleString('en-US', options2);
            },
          },
          {
            name: '',
            orderable: false,
            render: function (_data, type, row) {
              const buyId = row.BuyId;
              // tslint:disable-next-line:max-line-length
              const customHTML = ' <a view-editOrder-id="' + buyId + '" ng-click="editOrder(' + buyId + ');" href="javascript:" title="Edit" style="width:75px;text-align:center;">' +
                '<img view-editOrder-id="' + buyId + '" src="' + root + '/' + imgurl + '/edit-icon.png" alt="" /> </a>';
              return customHTML;
            },
          },
        ],
      };
      if (!this.isMediaAssets_DataLoad_FirstTime) {
        setTimeout(() => {
          try {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.proposalTableDatatableTrigger.next();
            });
          } catch (error) {
            this.hideLoader();
            console.log(error);
          }
        }, 1000);
        this.hideLoader();
      }

    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }


  // --- Export :: Excel & PDF ---

  exportToFile() {
    const file = (<HTMLSelectElement>document.getElementById('ddlpvExport')).value;
    if (file === 'Excel') {
      this.exporttoExcel();
    } else if (file === 'PDF') {
      this.exportToPdf();
    }
  }

  exporttoExcel() {
    const pOrders = [];

    this.tmpAllProposalOrders.forEach((data, key) => {
      this.obj = {};
      this.obj.BuyId = data.BuyId;
      this.obj.AdvertiserName = data.AdvertiserName;
      this.obj.billToContactName = data.BillToContactName;
      this.obj.OrderStatus = data.OrderStatus;
      this.obj.GrossCost = '$' + (parseFloat(data.GrossCost)).toFixed(2);
      this.obj.NetCost = '$' + (parseFloat(data.NetCost)).toFixed(2);
      pOrders.push(this.obj);
    });
    alasql('SELECT * INTO XLSX("ProposalOrders.xlsx",{headers:true}) FROM ?', [pOrders]);
  }

  exportToPdf() {
    try {
      this.tmpProposalOrdersForAutoTable = [];

      this.tmpAllProposalOrders.forEach((data, key) => {
        this.obj = [];
        this.obj.push(data.BuyId);
        this.obj.push(data.AdvertiserName);
        this.obj.push(data.BillToContactName);
        this.obj.push(data.OrderStatus);
        this.obj.push('$' + (parseFloat(data.GrossCost)).toFixed(2));
        this.obj.push('$' + (parseFloat(data.NetCost)).toFixed(2));
        this.tmpProposalOrdersForAutoTable.push(this.obj);
      });
      const columns = ['Buy Id', 'Advertiser', 'Billing Contact', 'Status', 'Gross Cost', 'Net Cost'];
      // Only pt supported (not mm or in)
      const doc = new jsPDF('p', 'pt');
      doc.autoTable(columns, this.tmpProposalOrdersForAutoTable, {
        theme: 'plain'
      });
      doc.save('ProposalOrders.pdf');
    } catch (e) {
      this.hideLoader();
      console.log(e);
    }
  }


  showLoader() {
    this.appComponent.isLoading = true;
  }

  hideLoader() {
    setTimeout(() => {
      this.appComponent.isLoading = false;
    }, 500);
  }
}
