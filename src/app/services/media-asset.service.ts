import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MediaAssetService {


  public headers: HttpHeaders;
  public sfheaders: HttpHeaders;
  authToken = environment.token;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
    });
    this.sfheaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'RequestVerificationToken': this.authToken
    });
  }

  getAll(): Observable<any> {
    return this.http.get(`${environment.ApiBaseURL}/MediaAsset`);
  }

  GetordersformediaAsset(dataTablesParameters, filterOptionsdata): Observable<any> {
    try {
      const form_data = new FormData();

      // tslint:disable-next-line:forin
      for (const key in dataTablesParameters) {
        form_data.append(key, dataTablesParameters[key]);
      }

      return this.http.post(`${environment.ApiBaseURL}/Dashboard/GetOrderDetailsByMediaAssets`, form_data);
    } catch (error) {
      console.log(error);
    }
  }
}
