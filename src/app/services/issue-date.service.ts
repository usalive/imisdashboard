import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class IssueDateService {

  baseAPI = environment.ApiBaseURL;
  issueDateApi = this.baseAPI + 'IssueDate';

  public headers: HttpHeaders;
  public sfheaders: HttpHeaders;
  authToken = environment.token;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
    });
    this.sfheaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'RequestVerificationToken': this.authToken
    });
  }

  getAll(): Observable<any> {
    return this.http.get(this.issueDateApi);
  }

}
