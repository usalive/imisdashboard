import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashBoardService {

  baseAPI = environment.ApiBaseURL;
  dashboardApi = this.baseAPI + '/Dashboard';

  public headers: HttpHeaders;
  public sfheaders: HttpHeaders;
  authToken = environment.token;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
    });
    this.sfheaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'RequestVerificationToken': this.authToken
    });
  }

  getDashboardData(): Observable<any> {
    return this.http.get(this.dashboardApi + '/GetDashboardData', { headers: this.sfheaders });
  }


  getInserationOrderByStatus(data): Observable<any> {
    const body = JSON.stringify(data);
    return this.http.post(this.dashboardApi + '/GetInsertionOrderCountByOrderStatus', body, { headers: this.sfheaders });
  }

}
