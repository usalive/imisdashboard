import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AdvertiserService {

  baseAPI = environment.ApiBaseURL;


  public headers: HttpHeaders;
  public sfheaders: HttpHeaders;
  authToken = environment.token;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
    });
    this.sfheaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'RequestVerificationToken': this.authToken
    });
  }

  getOrdersforadvertiser(dataTablesParameters, filterOptionsdata): Observable<any> {
    try {
      const form_data = new FormData();

      // tslint:disable-next-line:forin
      for (const key in dataTablesParameters) {
        form_data.append(key, dataTablesParameters[key]);
      }

      form_data.append('AsiPartyAPI', filterOptionsdata['AsiPartyAPI']);
      return this.http.post(`${environment.ApiBaseURL}/Dashboard/GetOrderByAdvertisers`, form_data);
    } catch (error) {
      console.log(error);
    }
  }


}
