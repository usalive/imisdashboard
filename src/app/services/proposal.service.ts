import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProposalService {

  baseAPI = environment.ApiBaseURL;

  public headers_token: HttpHeaders;
  public headers: HttpHeaders;
  authToken = environment.token;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
    });
  }

  getDataForPostOrdersDropDown(data, authToken): Observable<any> {

    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });

    const body = JSON.stringify(data);
    return this.http.post(`${environment.ApiBaseURL}/MediaOrder/FillDropDownForPostOrder`, body, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  getAdvertisersByIds(websiteRoot, authToken, AdvertisersIds): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      return this.http.get(websiteRoot + `api/Party?Id=in:` + AdvertisersIds + '', { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  getProposalStatus (data, authToken): Observable<any> {
    try {
      const form_data = new FormData();

      // tslint:disable-next-line:forin
      for (const key in data) {
        form_data.append(key, data[key]);
      }

      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      return this.http.post(`${environment.ApiBaseURL}/Dashboard/GetOrdersByProposalStatus`, form_data, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  getOrdersbyProposalStatus(dataTablesParameters, filterOptionsdata): Observable<any> {
    try {
      const form_data = new FormData();

      // tslint:disable-next-line:forin
      for (const key in dataTablesParameters) {
        form_data.append(key, dataTablesParameters[key]);
      }
      // tslint:disable-next-line:forin
      for (const key in filterOptionsdata) {
        if ( filterOptionsdata[key] === undefined ||  filterOptionsdata[key] == null) {
          form_data.append(key, '');
        } else {
          form_data.append(key, filterOptionsdata[key]);
        }
      }
      return this.http.post(`${environment.ApiBaseURL}/Dashboard/GetOrdersByProposalStatus`, form_data);
    } catch (error) {
      console.log(error);
    }
  }

}
