import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MediaOrderService {

  baseAPI = environment.ApiBaseURL;
  mediaOrderApi = this.baseAPI + 'MediaOrder';


  public headers: HttpHeaders;
  public sfheaders: HttpHeaders;
  authToken = environment.token;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
    });
    this.sfheaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'RequestVerificationToken': this.authToken
    });
  }


  getDataForPostOrdersDropDown(data): Observable<any> {
    const body = JSON.stringify(data);

    return this.http.post(`${environment.ApiBaseURL}/MediaOrder/FillDropdownForPostOrder`, body, { headers: this.sfheaders });
  }
}
